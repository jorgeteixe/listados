---
layout: post
section: others
title: HackLab Almería
---

## Información

HackLab Almería es un colectivo de experimentación tecnológica, social y creativa: desarrolladores, diseñadores, fabricantes, periodistas y aficionados reunidos para divertirse con la tecnología. Es el lugar perfecto donde experimentar y hacer realidad proyectos nuevos por medio de grupos de trabajo, poniendo en contacto a expertos en diferentes materias.

-   Temáticas principales: software libre, tecnología, cultura libre

## Contacto

-   Sede física:
-   Página web: <https://hacklabalmeria.net/>
-   Email:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/HackLabAl>
-   Sala en Matrix:
-   Grupo o canal en Telegram: <https://t.me/HackLab_Almeria_charla>
-   GitLab/GitHub (u otros sitios de código colaborativo): <https://github.com/HackLab-Almeria>
