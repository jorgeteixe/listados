---
layout: post
section: others
title: Club Robótica Granada
---

## Información

Somos una organización de ayuda a la difusión y democratización de la tecnología, la programación y la robótica en la sociedad, constituida por personas entusiastas de la tecnología y la ciencia. Nos centramos sobre todo en el uso de herramientas libres para la difusión y pensamos que el conocimiento debe ser libre, por eso realizamos talleres y cursos para todos los socios y no socios del club.

-   Temáticas principales: Robótica, programación, tecnología, impresión 3D....

## Contacto

-   Sede física:
   -   Open LAB - Parque de las Ciencias de Andalucía: Av. de la Ciencia, s/n, 18006 Granada
   -   OAL de Promoción y Empleo Ayuntamiento de Albolote: C/ Jacobo Camarero, 0, 18220 Albolote (Granada)
-   Página web: <https://clubroboticagranada.github.io/>
-   Email: <clubroboticagranada@gmail.com>
-   Mastodon (u otras redes sociales libres):
-   Twitter: [@clubroboticagra](https://twitter.com/clubroboticagra)
-   Sala en Matrix:
-   Grupo o canal en Telegram:
-   GitLab/GitHub (u otros sitios de código colaborativo): <https://github.com/clubroboticagranada>
