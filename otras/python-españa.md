---
layout: post
section: others
title: Python España
---

## Información

Asociación sin ánimo de lucro cuyo propósito es promover el uso del lenguaje de programación Python en España, servir como punto de encuentro a aquellos interesados en su uso y darles soporte en la medida de sus posibilidades. Fue creada en 2013 y desde entonces ha prestado apoyo contable y organizativo a la PyConES, la conferencia nacional sobre el lenguaje Python, y ayuda financiera a las comunidades locales.

-   Temáticas principales: Python

## Contacto

-   Sede física:
-   Página web: <https://es.python.org/>
-   Email:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/python_es>
-   Sala en Matrix:
-   Grupo o canal en Telegram: <https://t.me/PythonEsp>
-   GitLab/GitHub (u otros sitios de código colaborativo): <https://github.com/python-spain>
