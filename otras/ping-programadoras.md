---
layout: post
section: others
title: Ping a Programadoras
---

## Información

Asociación de mujeres y personas de género no binario profesionales en tecnología, que desde que nació el 2016 en Sevilla organiza eventos de diversa naturaleza con la intención de romper con la brecha de género en la tecnología: la proyección de documentales relacionados, talleres de introducción a tecnologías, talleres más avanzados o femiciclos.

-   Temáticas principales: Feminismo y tecnología

## Contacto

-   Sede física:
-   Página web: <https://pingprogramadoras.org/>
-   Email:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/pingmujeres>
-   Sala en Matrix:
-   Grupo o canal en Telegram:
-   GitLab/GitHub (u otros sitios de código colaborativo): <https://github.com/pingaprogramadoras>
