---
layout: post
section: associations
title: Aula de Software Libre de la Universidad de Córdoba
---

## Información

-   Universidad a la que está asociada: Universidad de Córdoba
-   Activa en la actualidad: Sí

## Contacto

-   Sede física:
-   Página web: <https://www.uco.es/aulasoftwarelibre/>
-   Email:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/AulaSL>
-   Sala en Matrix:
-   Grupo o canal en Telegram:
-   GitLab/GitHub (u otros sitios de código colaborativo):
