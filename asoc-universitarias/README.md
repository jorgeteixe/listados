# Asociaciones Estudiantiles Universitarias Libres

En <https://listados.eslib.re/asociaciones/> puedes encontrar listadas las páginas con información de las siguientes asociaciones estudiantes universitarias que tienen como objetivo la divulgación de tecnologías y cultura libres a través de sus actividades:

-   [Asociación de Software Libre de la Universidad de Huelva](asl-uhu.md)
-   [Aula de Software Libre de la Universidad de Córdoba](asl-uco.md)
-   [Grupo de Usuarios de Linux de la Universidad Carlos III de Madrid](gul-uc3m.md)
-   [Grupo de usuarios/as de GNU/Linux de la Universidad de Sevilla](sugus-us.md)
-   [LibreLabUCM](librelabucm.md)
-   [Oficina de Software Libre de la Asociación de Universitarios Informáticos de la Universidad de Almería](osl-unia.md)
-   [Grupo de Programadores e Usuarios de Linux de la Universidad de Coruña](gpul-udc.md)

Si conoces alguna que no aparece aquí puedes añadirla directamente en este mismo archivo, pero si puedes, te agradeceríamos que añadieras en esta misma carpeta su página de información usando [esta plantilla](../plantillas/asociaciones.md).
