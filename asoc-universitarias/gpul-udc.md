---
layout: post
section: associations
title: Grupo de Programadores e Usuarios de Linux
---

## Información

-   Universidad a la que está asociada: Universidade da Coruña
-   Activa en la actualidad: Sí

## Contacto

-   Sede física: Facultad Informática, Campus Elviña, 15071, A Coruña
-   Página web: <https://gpul.org/>
-   Email: <mailto:info@gpul.org>
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/gpul_>
-   Sala en Matrix: <https://riot.im/app/#/room/#gpul:matrix.org>
-   Grupo o canal en Telegram: <https://t.me/+PrK44xhqJ9eP9ZUf>
-   GitLab/GitHub (u otros sitios de código colaborativo): <https://github.com/gpul-org>
