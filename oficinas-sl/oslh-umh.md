---
layout: post
section: offices
title: Oficina de Software y Hardware Libre de la Universidad Miguel Hernández
---

## Información

-   Director/a:
-   Email:
-   Activa en la actualidad: Sí

## Contacto

-   Dirección física:
-   Página web: <https://oshl.umh.es/>
-   Email:
-   Teléfono:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/oshlumh>
-   Sala en Matrix:
-   Grupo o canal en Telegram:
-   GitLab/GitHub (u otros sitios de código colaborativo):
