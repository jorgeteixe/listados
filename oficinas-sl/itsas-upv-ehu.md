---
layout: post
section: offices
title: ITSAS - Grupo de Software Libre de la Universidad del País Vasco/Euskal Herriko Unibertsitatea
---

## Información

-   Director/a:
-   Email:
-   Activa en la actualidad: No

## Contacto

-   Dirección física:
-   Página web: <https://www.ehu.eus/es/web/itsas/home>
-   Email:
-   Teléfono:
-   Mastodon (u otras redes sociales libres):
-   Twitter:
-   Sala en Matrix:
-   Grupo o canal en Telegram:
-   GitLab/GitHub (u otros sitios de código colaborativo):
