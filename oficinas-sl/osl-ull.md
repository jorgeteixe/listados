---
layout: post
section: offices
title: Oficina de Software Libre de la Universidad de La Laguna
---

## Información

-   Director/a:
-   Email:
-   Activa en la actualidad: Sí

## Contacto

-   Dirección física:
-   Página web: <https://www.ull.es/servicios/osl/>
-   Email:
-   Teléfono:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/oslull>
-   Sala en Matrix:
-   Grupo o canal en Telegram:
-   GitLab/GitHub (u otros sitios de código colaborativo):
