---
layout: post
section: offices
title: Coneixement Obert i Programari Lliure a la Universitat d’Alacant
---

## Información

-   Director/a:
-   Email:
-   Activa en la actualidad: No

## Contacto

-   Dirección física:
-   Página web: <https://blogs.ua.es/copla/>
-   Email:
-   Teléfono:
-   Mastodon (u otras redes sociales libres):
-   Twitter:
-   Sala en Matrix:
-   Grupo o canal en Telegram:
-   GitLab/GitHub (u otros sitios de código colaborativo):
