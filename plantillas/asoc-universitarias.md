---
layout: post
section: associations
title: NOMBRE
---

## Información

[Algo de información sobre la misma]

-   Universidad a la que está asociada:
-   Activa en la actualidad:

## Contacto

-   Sede física:
-   Página web:
-   Email:
-   Mastodon (u otras redes sociales libres):
-   Twitter:
-   Sala en Matrix:
-   Grupo o canal en Telegram:
-   GitLab/GitHub (u otros sitios de código colaborativo):
