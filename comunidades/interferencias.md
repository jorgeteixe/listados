---
layout: post
section: communities
title: Interferencias
---

## Información

Asociación ciberactivista dedicada a organizar actividades y compartir información y opinión sobre derechos digitales, seguridad informática y temáticas similares, además de con el objetivo de promover el software libre.

-   Temáticas principales: derechos digitales, privacidad en Internet, seguridad informática

## Contacto

-   Sede física:
-   Página web: <https://interferencias.tech/>
-   Email: <mailto:info@interferencias.tech>
-   Mastodon (u otras redes sociales libres): <https://mastodon.technology/@interferencias>
-   Twitter: <https://twitter.com/Inter_ferencias>
-   Sala en Matrix:
-   Grupo o canal en Telegram: <https://t.me/inter_ferencias>
-   GitLab/GitHub (u otros sitios de código colaborativo): <https://gitlab.com/interferencias>
