---
layout: post
section: communities
title: LibreLabGRX
---

## Información

El objetivo de este grupo es dar cabida a actividades relacionadas con software/hardware libre, cultura y tecnologías abiertas, así como el desarrollo de lo que esto implica: hackathones, programación y todo lo geek de diferentes asociaciones y grupos de usuarios independientes ubicados en Granada.

-   Temáticas principales: software libre, hardware libre, cultura libre

## Contacto

-   Sede física:
-   Página web: <https://librelabgrx.cc/>
-   Email: <mailto:info@librelabgrx.cc>
-   Mastodon (u otras redes sociales libres): <https://floss.social/@librelabgrx>
-   Twitter: <https://twitter.com/LibreLabGRX/>
-   Sala en Matrix:
-   Grupo o canal en Telegram: <https://t.me/LibreLabGRX>
-   GitLab/GitHub (u otros sitios de código colaborativo): <https://gitlab.com/librelabgrx>
