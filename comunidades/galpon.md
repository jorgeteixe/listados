---
layout: post
section: communities
title: GALPon - Grupo de Amigos de Linux de Pontevedra
---

## Información

-   Temáticas principales: Promoción y desarrollo del software libre y la cultura libre en general

## Contacto

-   Sede física: Rúa Uruguai 15 entresollado B, 36201 Vigo (Pontevedra)
-   Página web: <https://www.galpon.org/>
-   Email: <info@galpon.org>
-   Mastodon (u otras redes sociales libres): [@galpon@mastodon.technology](https://mastodon.technology/@galpon) (sin uso actualmente)
-   Twitter: <https://twitter.com/galpon>
-   Sala en Matrix: [#galpon:matrix.org](https://matrix.to/#/#galpon:matrix.org)
-   Grupo o canal en Telegram: [@galpon](https://t.me/galpon)
-   GitLab/GitHub (u otros sitios de código colaborativo): <https://github.com/orgs/galpon/>
